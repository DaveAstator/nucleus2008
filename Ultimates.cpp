//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include "aboutfrm.h"
#pragma hdrstop

#include "Ultimates.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


        const TColor clPos=RGB(255,64,64);
        const TColor clNeg=RGB(128,198,255);
        const TColor clNeut=RGB(164,164,164);
        const TColor clAtt=RGB(164,255,164);
        const TColor clDist=RGB(164,0,164);
TColor ChargeC[5]={RGB(255,64,64),RGB(128,198,255),RGB(164,164,164),RGB(164,255,164),RGB(164,0,164)};
int pstats[6]={0,0,0,0,0,0};

void TForm1::CollideBoth(particle &pt1, particle &pt2)
{
       //TODO: Add your source code here
}

void TForm1::AffectLeft(particle & pt1, particle & pt2)
{

double angbb,angbb1,angrb,angrb1,vsum,vsum2;//,vxto1,vyto1;
pt1.setang();
pt1.setvel();
pt2.setang();
pt2.setvel();

// need to use projections FUCK THIS!
float overlap=(pt1.rad+pt2.rad-distp(pt1,pt2));
float speedportion=0;
if (overlap>0) {//overlap=0;
//part of overlap taken by pt1 and there fore distance it must travel back;
double dx=pt1.x-pt2.x;
double dy=pt1.y-pt2.y;
double dvx=pt1.xvel-pt2.xvel;
double dvy=pt1.yvel-pt2.yvel;
double a=dvx*dvx+dvy*dvy;
double b=-2*(dvx*dx+dvy*dy);
double c=(dx*dx)+(dy*dy)-((pt1.rad+pt2.rad)*(pt1.rad+pt2.rad));
double D=(b*b)-(4*a*c);
float t=((-b+sqrt(fabs(D)))/(2*a));
if (t>1) t=0;


how many projections are in our overlap part
speedportion=t;// backspeed/pt1.vel;

pt1.x=pt1.x-pt1.xvel*speedportion;
pt1.y=pt1.y-pt1.yvel*speedportion;

         }
angbb=atanxy(pt1.x-pt2.x,pt1.y-pt2.y);//angle between ptcs considering it's coming out of pt2
angbb1=atanxy(pt2.x-pt1.x,pt2.y-pt1.y);//angle between ptcs considering it's coming out of pt1
angrb=angbb-pt2.ang;
angrb1=angbb1-pt1.ang;

if (fabs(angrb)>M_PI_2) angrb=M_PI_2;  //ang between p2's speed and bb angle(bb is from pt2)
if (fabs(angrb1)>M_PI_2) angrb1=M_PI_2;
float ptop=pt2.mass/pt1.mass;
if (!pt2.stationary) // if it is NOT STATIONARY
vsum=abs(pt2.vel*cos(angrb)*ptop)*cMul; //amount of affecting strength.
else
vsum=0;

vsum2=abs(pt1.vel*cos(angrb1));

//vsum=abs(pt1.vel*sin(angrb));
//projection of p2's speed on colLine
// reflect it back
// vsum=abs(pt2.vel*sin(angrb)*(pt2.mass+0.001)); //projection of p2's speed on colLine
// pt1.pressure+=(pt2.vel*cos(angrb)*pt2.mass+pt1.vel*cos(angrb1)*pt1.mass)*((pt1.rad*pt1.rad*M_PI)/pt1.mass);

if (ptop>2) pt1.ang=pt1.ang-(M_PI-angrb1*2);
pt1.vel-=vsum2;
pt1.setvxy();

//pt1.xvel=pt1.xvel-vsum2*cos(angbb1);
//pt1.yvel=pt1.yvel-vsum2*sin(angbb1);

pt1.xvel+=vsum*cos(angbb);
pt1.yvel+=vsum*sin(angbb);

pt1.setang();
pt1.x=pt1.x+pt1.xvel*speedportion;
pt1.y=pt1.y+pt1.yvel*speedportion;


//vyto1=vsum*sin(angbb);
        //TODO: Add your source code here

}


void __fastcall TForm1::FormCreate(TObject *Sender)
{

EndArrBut->Caption=(char)0xA5;
cMul=0.99;
cMulEd->Text=FloatToStr(cMul);
emode = 2;
bndX=0;
bndY=0;
bndR=1000;
qMul=512;
qMulEd->Text=FloatToStr(qMul);
frKoeff=1;
fMulEd->Text=FloatToStr(frKoeff);
vKoeff=160;
vDivEd->Text=FloatToStr(vKoeff);
QprCombo->ItemIndex=0;
offX=0;
offY=0;
actW=TheBoard->Width;
actH=TheBoard->Height;
visW=actW;
visH=actH;
offX=-visW/2;
offY=-visH/2;
going=false;
parr=new pstack;
lastpt=parr;
navarr=parr;
navid=1;
pnum=0;

TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
TheBoard->Parent->DoubleBuffered=true;
NavImage->Canvas->Brush->Color=clBlack;
NavImage->Canvas->FillRect(ClientRect);
NavImage->Parent->DoubleBuffered=true;
DrawStep=0;
//--------------
offX=offX+(actW-visW)/2;
offY=offY+(actH-visH)/2;
actW=visW*ZoomBar->Position/(ZoomBar->Max/6);
actH=visH*ZoomBar->Position/(ZoomBar->Max/6);
offX=offX-(actW-visW)/2;
offY=offY-(actH-visH)/2;
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TheBoardMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
if ((emode == 2) && !going){
   if (DrawStep==0) {
      //first click: creating empty place for nex pt;
      lastpt->next=new pstack;
      pnum++;
//      Label2->Caption=IntToStr(pnum);
      lastpt->pt.qprop=QprCombo->ItemIndex;
      pstats[QprCombo->ItemIndex]++;

   }
   if (DrawStep<3) {
      DrawStep++;
      pX=X;
      pY=Y;
   }
   else {
      lastpt->pt.setang() ;
      lastpt->pt.stationary=pPropsEd->Checked[0];
      if (pPropsEd->Checked[0]) {
      lastpt->pt.xvel=0;
      lastpt->pt.yvel=0;
      }
      SetPt=lastpt->pt;
      lastpt=lastpt->next;
      DrawStep=0;
      pRadEd->Text=FloatToStr(SetPt.rad);
   }
   if (DrawStep==1) {
      //Begin of drawing pt size;
      TheBoard->Canvas->Pen->Style=psClear;
      oX=X;
      oY=Y;
      pX=X;
      pY=Y;
      lastpt->pt.x=X*actW/visW+offX;
      lastpt->pt.y=Y*actH/visH+offY;
   }
   if (DrawStep==2) {
         //Begin of drawing pt Efield; Assigning mass by radius
//and mass here::::
        lastpt->pt.mass=0.11*lastpt->pt.rad*lastpt->pt.rad*M_PI+0.001;
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Color=clNeg;
        TheBoard->Canvas->Pen->Style=psDash;
        TheBoard->Canvas->Pen->Mode=pmXor;
        double rad;
        rad=sqrt((pX-oX)*(pX-oX)+(pY-oY)*(pY-oY));
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
   }
   if (DrawStep==3) {
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Style=psSolid;
        TheBoard->Canvas->Pen->Color=clAtt;
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(pX,pY);
   }
}
if (emode == 1) {
      lastpt->next=new pstack;
      SetPt.rad=StrToFloat(pRadEd->Text);
      SetPt.mass=StrToFloat(pMassEd->Text);
      SetPt.q=StrToFloat(pQEd->Text);
      SetPt.qprop=QprCombo->ItemIndex;
      pstats[SetPt.qprop]++;
      SetPt.stationary=pPropsEd->Checked[0];
      lastpt->pt=SetPt;
      lastpt->pt.x=X*actW/visW+offX;
      lastpt->pt.y=Y*actH/visH+offY;
      lastpt->pt.setang();
      lastpt=lastpt->next;
      pnum++;
      DrawParticles(parr);
}

UpdateStatus();
}

//---------------------------------------------------------------------------
void __fastcall TForm1::TheBoardMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
if (emode == 2) {
   if (DrawStep==1){
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmXor;
        TheBoard->Canvas->Brush->Color=ChargeC[QprCombo->ItemIndex];
        double rad;
        rad=sqrt(pow(pX-oX,2)+pow(pY-oY,2))/2;
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
        rad=sqrt(pow(X-oX,2)+pow(Y-oY,2))/2;
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);

        lastpt->pt.rad=rad*actW/visW;
        pRadEd->Text=FloatToStr(rad*actW/visW);
// mass here::
        pMassEd->Text=FloatToStr(4/3*Power(rad*actW/visW,3)*M_PI);
   }
   else
   if (DrawStep==2){
//setting the field;
          TheBoard->Canvas->Brush->Style=bsClear;
          TheBoard->Canvas->Pen->Color=clNeg;
          TheBoard->Canvas->Pen->Style=psDash;
          TheBoard->Canvas->Pen->Mode=pmXor;
        double rad;
          rad=sqrt(pow(pX-oX,2)+pow(pY-oY,2));
          TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
          rad=sqrt(pow(X-oX,2)+pow(Y-oY,2));
          TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
        if (lastpt->pt.qprop== 1) lastpt->pt.q=-rad*actH/visH;
        else
           lastpt->pt.q=rad*actW/visW;
        pQEd->Text=FloatToStr(lastpt->pt.q);
   }
   if (DrawStep==3){
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Style=psSolid;
        TheBoard->Canvas->Pen->Color=clAtt;
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(pX,pY);
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(X,Y);
        lastpt->pt.xvel=(X-oX)*actW/visW;
        lastpt->pt.yvel=(Y-oY)*actH/visH;

      }
}

        pX=X;
        pY=Y;
}
//---------------------------------------------------------------------------



double TForm1::atanxy(double x, double y)
{
double ang;
        if (x==0 && y==0) ang=0;
          else
        if (x==0)
                ang=M_PI_2+(-Sign(y)+1)/2*M_PI;
          else
        if (x>0 && y<0)
                ang=M_PI*2+atan(y/x);
          else
        if (x<0)
                ang=M_PI+atan(y/x);
          else
                ang=atan(y/x);
return ang;
        //TODO: Add your source code here
}

bool TForm1::chkcollide(particle & pt1,particle & pt2)
{
        if (dist(pt1.x,pt1.y,pt2.x,pt2.y) < (pt1.rad+pt2.rad+1)){
                if (dist(pt1.x+pt1.xvel/vKoeff,pt1.y+pt1.yvel/vKoeff,pt2.x+pt2.xvel/vKoeff,pt2.y+pt2.yvel/vKoeff) < (pt1.rad+pt2.rad+1))
                return true;
}
  return false;
}

double TForm1::dist(double x1, double y1, double x2, double y2)
{
return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
        //TODO: Add your source code here
}
void __fastcall TForm1::CommitButClick(TObject *Sender)
{
TheBoard->Parent->DoubleBuffered=true;
parr->next->pt.rad=7;
}
//---------------------------------------------------------------------------


void TForm1::DrawParticles(pstack *darr)
{
int i;
double dx,dy,dr;
pstack *tmp;
tmp=darr;
for (i=0;i<pnum;i++){
if (!((tmp->pt.x+tmp->pt.rad)<offX) || ((tmp->pt.x+tmp->pt.rad)>(offX+actW)) || ((tmp->pt.y+tmp->pt.rad)<(offY)) || ((tmp->pt.y+tmp->pt.rad)>(offY+actH))){
      if(!drTrails->Checked)  TheBoard->Canvas->Brush->Color=clBlack;
        TheBoard->Canvas->Brush->Style=bsSolid;
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmCopy;
        dx=(tmp->pt.px-offX)*visW/actW;
        dy=(tmp->pt.py-offY)*visH/actH;
        dr=tmp->pt.rad*visW/actW;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        TheBoard->Canvas->Brush->Color=ChargeC[tmp->pt.qprop];
        dx=(tmp->pt.x-offX)*visW/actW;
        dy=(tmp->pt.y-offY)*visH/actH;
        dr=tmp->pt.rad*visW/actW;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        if (drFields->Checked || drFieldsLT){
            TheBoard->Canvas->Brush->Style=bsClear;
            TheBoard->Canvas->Pen->Color=clNeg;
            TheBoard->Canvas->Pen->Style=psDash;
            TheBoard->Canvas->Pen->Mode=pmXor;
            double rad;
            rad=abs(tmp->pt.q/actH*visH);
            if (!drFieldsFT){
            dx=(tmp->pt.px-offX)*visW/actW;
            dy=(tmp->pt.py-offY)*visH/actH;
            TheBoard->Canvas->Ellipse(dx-rad,dy-rad,dx+rad,dy+rad);
            }
            dx=(tmp->pt.x-offX)*visW/actW;
            dy=(tmp->pt.y-offY)*visH/actH;
            TheBoard->Canvas->Ellipse(dx-rad,dy-rad,dx+rad,dy+rad);
         }
         if (drVels->Checked){
            TheBoard->Canvas->Brush->Style=bsClear;
            TheBoard->Canvas->Pen->Color=clAtt;
            TheBoard->Canvas->Pen->Style=psSolid;
            TheBoard->Canvas->Pen->Mode=pmXor;
            dx=(tmp->pt.px-offX)*visW/actW;
            dy=(tmp->pt.py-offY)*visH/actH;
            TheBoard->Canvas->MoveTo(dx,dy);
            TheBoard->Canvas->LineTo(tmp->pt.xvel*visW/actW+dx ,tmp->pt.yvel*visW/actW+dy);
            dx=(tmp->pt.x-offX)*visW/actW;
            dy=(tmp->pt.y-offY)*visH/actH;
            TheBoard->Canvas->MoveTo(dx,dy);
            TheBoard->Canvas->LineTo(tmp->pt.xvel*visW/actW+dx ,tmp->pt.yvel*visW/actW+dy);

         }

}
       // OLD CODE.

/* code with centers
if (!((tmp->pt.x+tmp->pt.rad)<cenX-visW*zoom/2)
|| ((tmp->pt.x-tmp->pt.rad)>cenX+visW*zoom/2)
|| ((tmp->pt.y+tmp->pt.rad)<cenY-visH*zoom/2)
|| ((tmp->pt.y-tmp->pt.rad)>cenY+visH*zoom/2)){
int offX=cenX-
int actx,acty;
actx=pt.x-cenX-visW*zoom/2;
acty=pt.y-cenY-visH*zoom/2;
        TheBoard->Canvas->Brush->Color=clBlack;
        TheBoard->Canvas->Brush->Style=bsSolid;
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmCopy;
        dx=actx*zoom;
        dy=acty*zoom;
        dr=tmp->pt.rad*zoom;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        TheBoard->Canvas->Brush->Color=ChargeC[tmp->pt.qprop];
        dx=(tmp->pt.x-offX)*visW/actW;
        dy=(tmp->pt.y-offY)*visH/actH;
        dr=tmp->pt.rad*zoom;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
}                   */
tmp=tmp->next;
    }
                if (drFieldsFT) drFieldsFT=false;
//draw the bound;
    TheBoard->Canvas->Brush->Style=bsClear;
    TheBoard->Canvas->Pen->Style=psSolid;
    TheBoard->Canvas->Pen->Mode=pmCopy;
    TheBoard->Canvas->Pen->Color=clWhite;
    TheBoard->Canvas->Ellipse((bndX-offX-bndR)*visW/actW,(bndY-offY-bndR)*visW/actW,(bndX-offX+bndR)*visW/actW,(bndY-offY+bndR)*visW/actW);
// draw Scale;
TheBoard->Canvas->Pen->Color=clYellow;
TheBoard->Canvas->MoveTo(50,TheBoard->Height-10);
TheBoard->Canvas->LineTo(50+(30*visW/actW),TheBoard->Height-10);
TheBoard->Canvas->Pen->Color=clFuchsia;
TheBoard->Canvas->MoveTo(50,TheBoard->Height-20);
TheBoard->Canvas->LineTo(50+30,TheBoard->Height-20);
TheBoard->Canvas->Font->Size=8;
TheBoard->Canvas->Font->Color=clWhite;
TheBoard->Canvas->TextOutA(0,TheBoard->Height-24,"Vis/Real");
//TheBoard->Canvas->TextRect(20+(30*visW/actW)+5,TheBoard->Height-30,20+(30*visW/actW)+35,TheBoard->Height-10,"Real 30px");
}
void __fastcall TForm1::StartButClick(TObject *Sender)
{
going=StartBut->Down;
if (!going) StartBut->Caption="GO";
if (going) {
StartBut->Caption="STOP";
MakeAStep();
 }

}
//---------------------------------------------------------------------------



void TForm1::MakeAStep(void)
{
pstack *tmp,*fut,*tfut;
register int i,k; //theese ones go in ebx etc.
int z;
double chaos;
while (going) {
   chaos=0;
   tmp=parr;
   fut=new pstack;
   tfut=fut;
   for (i=0;i<pnum;i++){     //creating future system here:
      tmp->pt.px=tmp->pt.x;  //  setting pxand 'ys in present sys. i dunno - why so?..
      tmp->pt.py=tmp->pt.y;
      tfut->pt=tmp->pt;      //copy contents
      tfut->next=new pstack; //    create next free slot
      tmp=tmp->next;         //go forward
      tfut=tfut->next;
   }
   //after that step we have a copy of present system named fut.
   //going to start.
   tmp=parr;
   tfut=fut;
   for (i=0;i<pnum;i++){
      //affecting curr pt in future system by
      // every paticle in present
      for (k=0;k<pnum;k++){
         if (k!=i)  //affecting velocity by E-fields
        FieldAffectLeft(tfut->pt,tmp->pt);
        tmp=tmp->next;
      }

      tmp=parr;

      for (k=0;k<pnum;k++){
         tfut->pt.pressure=0;
         if (k!=i){  //affecting velocity by collison with pres particle and by E-fields
            if (!tfut->pt.stationary){
             if(chkcollide(tfut->pt,tmp->pt))
               AffectLeft(tfut->pt,tmp->pt);
            }
         }
      tmp=tmp->next;
      }

     if(chkcollideWBound(tfut->pt))
      if (!tfut->pt.stationary)
        AffectByBound(tfut->pt);

    tmp=parr; //go to beginning of present system
               //changing position of future particle
    tfut->pt.x=tfut->pt.x+tfut->pt.xvel/vKoeff;
    tfut->pt.y=tfut->pt.y+tfut->pt.yvel/vKoeff;

    tfut->pt.setvel();
    chaos=chaos+tfut->pt.vel*tfut->pt.mass;
//squeeze code
/*
if ((tfut->pt.mass/(tfut->pt.rad*tfut->pt.rad*M_PI))<(tfut->pt.pressure))
(tfut->pt.rad)=sqrt((tfut->pt.mass/(M_PI*tfut->pt.pressure)));
//---------*/

//double frsum;
//frsum=(1-frKoeff);
    tfut->pt.xvel=tfut->pt.xvel*(frKoeff);
    tfut->pt.yvel=tfut->pt.yvel*(frKoeff);

    tfut=tfut->next;          //changing to the next particle
}
//now we need to copy the contents of future into present:
  //delete parr;
  //parr=fut;
  //lastpt=parr;
tfut=fut;  //going to zeroes
tmp=parr;  //begin of pres which will be changed to the future
  for (k=0;k<pnum;k++){
      tmp->pt=tfut->pt;
      tmp=tmp->next;   //going forward.. WOW THAT WAS IT!
      tfut=tfut->next;
  }
//  lastpt->next=new pstack;
//  lastpt=lastpt->next;
Application->ProcessMessages();
DrawParticles(parr);
delete fut;
ChaosLabel->Caption="Chaos level: "+FloatToStr(chaos);
if (!going) break;
}


}
void __fastcall TForm1::ClearScrClick(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
}
//---------------------------------------------------------------------------


double TForm1::distp(particle p1, particle p2)
{
return sqrt(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2));
// distance between centers;
        //TODO: Add your source code here

}
void __fastcall TForm1::YscrollChange(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
offY=offY+Yscroll->Position-100;
Yscroll->Position=100;
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::XscrollChange(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
offX=offX+Xscroll->Position-100;
Xscroll->Position=100;
DrawParticles(parr);
}
//---------------------------------------------------------------------------


void TForm1::FieldAffectLeft(particle &pt1, particle &pt2)
{
double stren,angbb;
stren=((pt2.q*pt1.q*qMul)/pow(distp(pt1,pt2),2))/vKoeff;//*(pt2.mass/pt1.mass);
angbb=atanxy(pt2.x-pt1.x,pt2.y-pt1.y);
if ((pt1.qprop<2) && (pt2.qprop<2)){
   pt1.xvel=pt1.xvel-stren*cos(angbb);
   pt1.yvel=pt1.yvel-stren*sin(angbb);

}
else
if ((pt1.qprop*2==pt1.qprop+pt2.qprop) && (pt1.qprop<2)){
   pt1.xvel=pt1.xvel-abs(stren)*cos(angbb);
   pt1.yvel=pt1.yvel-abs(stren)*sin(angbb);

}
else
if (pt1.qprop==2 || pt2.qprop==2){}
else
if ( ((pt1.qprop==4) && (pt2.qprop==3)) || ((pt1.qprop==3) && (pt2.qprop==4)) ){
}
else
if ((pt1.qprop==3) || (pt2.qprop==3)){
   pt1.xvel=pt1.xvel+(abs(stren)*cos(angbb));
   pt1.yvel=pt1.yvel+(abs(stren)*sin(angbb));
}
else
if ((pt1.qprop==4) || (pt2.qprop==4)){
   pt1.xvel=pt1.xvel-(abs(stren)*cos(angbb));
   pt1.yvel=pt1.yvel-(abs(stren)*sin(angbb));
}




}

void __fastcall TForm1::Button2Click(TObject *Sender)
{
ZoomBar->Position=ZoomBar->Max/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ZoomBarChange(TObject *Sender)
{
offX=offX+(actW-visW)/2;
offY=offY+(actH-visH)/2;
actW=visW*ZoomBar->Position/(ZoomBar->Max/6);
actH=visH*ZoomBar->Position/(ZoomBar->Max/6);
offX=offX-(actW-visW)/2;
offY=offY-(actH-visH)/2;
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
DrawParticles(parr);
setNavProps();

}
//---------------------------------------------------------------------------


void __fastcall TForm1::vDivEdKeyPress(TObject *Sender, char &Key)
{
if (Key == 13) {
if (vDivEd->Text.Length()<1 || StrToFloat(vDivEd->Text)<=0 ){
  vDivEd->Text="1";
  vKoeff=1;
 }
 vKoeff=StrToFloat(vDivEd->Text);
vDivFlash->Brush->Color=clLime;
}
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}


}
//---------------------------------------------------------------------------


void __fastcall TForm1::fMulEdKeyPress(TObject *Sender, char &Key)
{
if (Key == 13) {
if (StrToFloat(fMulEd->Text)>1 || StrToFloat(fMulEd->Text)<=0 || fMulEd->Text.Length()<=0 ){
  fMulEd->Text="1";
 }
 frKoeff=StrToFloat(fMulEd->Text);
fMulFlash->Brush->Color=clLime;
}
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------





void __fastcall TForm1::qMulEdKeyPress(TObject *Sender, char &Key)
{
if (Key == 13) {
 if (fMulEd->Text.Length()<=0){
  qMulEd->Text="1";
 }
  qMul=StrToFloat(qMulEd->Text);
qMulFlash->Brush->Color=clLime;
}
if (Key == '.') Key =',';
if (Key == '-') {
qMul=-qMul;
qMulEd->Text=FloatToStr(qMul);
}
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------



bool TForm1::chkcollideWBound(particle & pt1)
{
         if (dist(pt1.x,pt1.y,bndX,bndY) > (bndR-pt1.rad-1)){
                if (dist(pt1.x+pt1.xvel/vKoeff,pt1.y+pt1.yvel/vKoeff,bndX,bndY) > ((bndR-pt1.rad-1)))
                return true;
}
  return false;

   //TODO: Add your source code here
}

void TForm1::AffectByBound(particle & pt1)
{
double angbb,angrb,vsum;//,vxto1,vyto1;
pt1.setang();
pt1.setvel();
angbb=atanxy(bndX-pt1.x,bndY-pt1.y);//angle between pt and bound coming out of bound centre

/* old bouncing code
//DO NOT ERASE!
pt1.vel*=cMul;
pt1.ang=pt1.ang-((pt1.ang-(angbb+M_PI_2))*2);
pt1.setvxy();
if (dist(pt1.x,pt1.y,bndX,bndY)>(bndR-pt1.rad)){
pt1.x=pt1.x+((dist(pt1.x,pt1.y,bndX,bndY)+pt1.rad-bndR)*cos(angbb));
pt1.y=pt1.y+((dist(pt1.x,pt1.y,bndX,bndY)+pt1.rad-bndR)*sin(angbb));
}
*/
double overlap=(dist(pt1.x,pt1.y,bndX,bndY)+pt1.rad-bndR);
pt1.xvel=pt1.xvel*cMul+overlap*cos(angbb);
pt1.yvel=pt1.yvel*cMul+overlap*sin(angbb);

}
void __fastcall TForm1::SpeedButton2Click(TObject *Sender)
{
emode=3;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton1Click(TObject *Sender)
{
emode=2;   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton3Click(TObject *Sender)
{
emode=1;   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BndREdKeyPress(TObject *Sender, char &Key)
{
if (Key == '.') Key =',';
if (Key == 13) {
 if (BndREd->Text.Length()<=0){
  BndREd->Text="1";
 }
  bndR=StrToFloat(BndREd->Text);
  BndRFlash->Brush->Color=clLime;
  ClearScr->Click();
}
DrawParticles(parr);
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::BndREdChange(TObject *Sender)
{
if (StrToFloat(BndREd->Text) != bndR)
BndRFlash->Brush->Color=clRed;
else
{
BndRFlash->Brush->Color=clLime;
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::QprComboSelect(TObject *Sender)
{
if (QprCombo->ItemIndex==0) pQEd->Text=FloatToStr(abs(StrToFloat(pQEd->Text)));
if (QprCombo->ItemIndex==1) pQEd->Text=FloatToStr(-abs(StrToFloat(pQEd->Text)));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SetCMulClick(TObject *Sender)
{
cMul=StrToFloat(cMulEd->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cMulEdKeyPress(TObject *Sender, char &Key)
{
if (Key == 13){
cMul=StrToFloat(cMulEd->Text);
cMulFlash->Brush->Color=clLime;
}
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::vDivEdChange(TObject *Sender)
{
if (StrToFloat(vDivEd->Text) != vKoeff)
vDivFlash->Brush->Color=clRed;
else
vDivFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::vDivX05Click(TObject *Sender)
{
vKoeff=vKoeff/2;
vDivEd->Text=FloatToStr(vKoeff);
vDivFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::vDivX2Click(TObject *Sender)
{
vKoeff=vKoeff*2;
vDivEd->Text=FloatToStr(vKoeff);
vDivFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::fMulEdChange(TObject *Sender)
{
if (StrToFloat(fMulEd->Text) != frKoeff)
fMulFlash->Brush->Color=clRed;
else
fMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button3Click(TObject *Sender)
{
frKoeff=frKoeff*2;
fMulEd->Text=FloatToStr(frKoeff);
fMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
frKoeff=frKoeff/2;
fMulEd->Text=FloatToStr(frKoeff);
fMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::vDivFlashMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
vDivEd->Text=FloatToStr(vKoeff);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::fMulFlashMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
fMulEd->Text=FloatToStr(frKoeff);   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::qMulFlashMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
qMulEd->Text=FloatToStr(qMul);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cMulFlashMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
cMulEd->Text=FloatToStr(cMul);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::qMulEdChange(TObject *Sender)
{
if (StrToFloat(qMulEd->Text) != qMul)
qMulFlash->Brush->Color=clRed;
else
qMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cMulEdChange(TObject *Sender)
{
if (StrToFloat(cMulEd->Text) != cMul)
cMulFlash->Brush->Color=clRed;
else
cMulFlash->Brush->Color=clLime;
 
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
qMul=qMul*2;
qMulEd->Text=FloatToStr(qMul);
qMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
qMul=qMul/2;
qMulEd->Text=FloatToStr(qMul);
qMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
cMul=cMul*2;
cMulEd->Text=FloatToStr(cMul);
cMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
cMul=cMul/2;
cMulEd->Text=FloatToStr(cMul);
cMulFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button9Click(TObject *Sender)
{
AboutForm->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button10Click(TObject *Sender)
{
if (going){
going=false;
StartBut->Down=false;
StartBut->Caption="GO";
}
UpdateStatus();
delete parr;
parr=new pstack;
pnum=0;
lastpt=parr;
navarr=parr;
navid=0;
ClearScr->Click();
DrawParticles(parr);
setNavProps();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
bndR=bndR*2;
BndREd->Text=FloatToStr(bndR);
BndRFlash->Brush->Color=clLime;
ClearScr->Click();
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button11Click(TObject *Sender)
{
bndR=bndR/2;
BndREd->Text=FloatToStr(bndR);
BndRFlash->Brush->Color=clLime;
ClearScr->Click();
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BndRFlashMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
BndREd->Text=FloatToStr(bndR);
BndRFlash->Brush->Color=clLime;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button12Click(TObject *Sender)
{
ZoomZoom(2);
}
//---------------------------------------------------------------------------


void TForm1::ZoomZoom(double k)
{
offX=offX+(actW-visW)/2;
offY=offY+(actH-visH)/2;
actW=actW*k;
actH=actH*k;
offX=offX-(actW-visW)/2;
offY=offY-(actH-visH)/2;
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
DrawParticles(parr);
setNavProps();
   //TODO: Add your source code here
}
void __fastcall TForm1::Button13Click(TObject *Sender)
{
ZoomZoom(3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button14Click(TObject *Sender)
{
ZoomZoom(0.5);   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button15Click(TObject *Sender)
{
ZoomZoom(0.3333333);   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::drFieldsClick(TObject *Sender)
{
if (drFields->Checked){
drFieldsFT=true;
}else
ClearScr->Click();

}
//---------------------------------------------------------------------------


void TForm1::UpdateStatus(void)
{
//StatusBar1->Font->Color=clRed;
StatusBar1->Panels->Items[0]->Text="Particles In Simulation: "+IntToStr(pnum);
StatusBar1->Panels->Items[1]->Text="Pos: "+IntToStr(pstats[0]);
//StatusBar1->Font->Color=clBlue;
StatusBar1->Panels->Items[1]->Text+="  |    Neg: "+IntToStr(pstats[1]);
//StatusBar1->Font->Color=clGray;
StatusBar1->Panels->Items[1]->Text+="  |    Empt: "+IntToStr(pstats[2]);
//StatusBar1->Font->Color=clGreen;
StatusBar1->Panels->Items[1]->Text+="  |    Att: "+IntToStr(pstats[3]);
//StatusBar1->Font->Color=clPurple;
StatusBar1->Panels->Items[1]->Text+="  |    Dist: "+IntToStr(pstats[4]);
   //TODO: Add your source code here
}




void __fastcall TForm1::frUpDownMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
frKoeff+=0.002*(frUpDown->Position-1);
fMulEd->Text=FloatToStr(frKoeff);
fMulFlash->Brush->Color=clLime;
frUpDown->Position=1;   
}
//---------------------------------------------------------------------------


void __fastcall TForm1::BegArrButClick(TObject *Sender)
{
navarr=parr;
navid=1;
setNavProps();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EndArrButClick(TObject *Sender)
{
int i;
navarr=parr;
for (i=1;i<pnum;i++)
navarr=navarr->next;
setNavProps();
navid=pnum;
setNavProps();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::PrvArrClick(TObject *Sender)
{
if (navid > 1) navid--;
int i;
navarr=parr;
for (i=1;i<navid;i++)
navarr=navarr->next;
setNavProps();
}
//---------------------------------------------------------------------------


void TForm1::setNavProps()
{
//modifying MEMO
Memo1->Lines->Strings[0]="Particle �"+IntToStr(navid);
Memo1->Lines->Strings[1]=QprCombo->Items->Strings[navarr->pt.qprop];
Memo1->Lines->Strings[2]="Rad: "+LeftStr(FloatToStr(navarr->pt.rad),6);
Memo1->Lines->Strings[3]="q: "+LeftStr(FloatToStr(navarr->pt.q),6);
if (navarr->pt.stationary)
Memo1->Lines->Strings[4]="--STATIONARY--";
else
Memo1->Lines->Strings[4]=" ";
// Drawing in Picture
        NavImage->Canvas->Brush->Color=clBlack;
        NavImage->Canvas->FillRect(ClientRect);
        NavImage->Canvas->Pen->Style=psClear;
        float dx,dy,dr;
        dr=navarr->pt.rad*visW/actW;
if (dr > NavImage->Width/2) dx=-dr+NavImage->Width/2;
else
        dx=NavImage->Width/2;
        dy=NavImage->Width/2;
        NavImage->Canvas->Brush->Color=ChargeC[navarr->pt.qprop];
        NavImage->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);

            NavImage->Canvas->Brush->Style=bsClear;
            NavImage->Canvas->Pen->Color=clNeg;
            NavImage->Canvas->Pen->Style=psDash;
            NavImage->Canvas->Pen->Mode=pmCopy;
            double rad;
            rad=abs(navarr->pt.q/actH*visH);
            dx=-rad+NavImage->Width/2+30;
            NavImage->Canvas->Ellipse(dx-rad,dy-rad,dx+rad,dy+rad);
}
void __fastcall TForm1::NxtArrButClick(TObject *Sender)
{
if (navid < pnum) {
navarr=navarr->next;
navid++;
setNavProps();
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button17Click(TObject *Sender)
{
pRadEd->Text=FloatToStr(navarr->pt.rad);
pQEd->Text=FloatToStr(navarr->pt.q);
pMassEd->Text=FloatToStr(navarr->pt.mass);
QprCombo->ItemIndex=navarr->pt.qprop;
pPropsEd->Checked[0]=navarr->pt.stationary;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button16Click(TObject *Sender)
{
/*if (going){
going=false;
StartBut->Down=false;
StartBut->Caption="GO";
} */
lastpt=parr;
register int i;
if (pnum>1)
{
   for (i=0;i<pnum-2;i++)
      lastpt=lastpt->next;
   delete lastpt->next;
   lastpt->next=new pstack;
   lastpt=lastpt->next;
   lastpt->next=new pstack;
   pnum--;
   if (navid == pnum) PrvArr->Click();
   UpdateStatus();
   ClearScr->Click();
   DrawParticles(parr);
   setNavProps();
   }
   else if (pnum==1)
      Button10->Click();

}

//---------------------------------------------------------------------------





