object Form1: TForm1
  Left = 261
  Top = 105
  Width = 806
  Height = 559
  Caption = 'Ultimate Particles'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 5
    Top = 64
    Width = 79
    Height = 74
    Shape = bsFrame
  end
  object TheBoard: TImage
    Left = 128
    Top = 16
    Width = 500
    Height = 500
    OnMouseDown = TheBoardMouseDown
    OnMouseMove = TheBoardMouseMove
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 104
    Width = 73
    Height = 22
    GroupIndex = 1
    Down = True
    Caption = 'Create mode'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 8
    Top = 128
    Width = 73
    Height = 22
    GroupIndex = 1
    Caption = 'Edit Mode'
    Visible = False
    OnClick = SpeedButton2Click
  end
  object CommitBut: TSpeedButton
    Left = 88
    Top = 75
    Width = 29
    Height = 54
    Caption = 'OK'
    OnClick = CommitButClick
  end
  object Bevel2: TBevel
    Left = 0
    Top = 184
    Width = 113
    Height = 9
    Shape = bsTopLine
  end
  object Label1: TLabel
    Left = 8
    Top = 264
    Width = 87
    Height = 13
    Caption = 'Charge Properties:'
  end
  object SpeedButton3: TSpeedButton
    Left = 8
    Top = 72
    Width = 73
    Height = 22
    GroupIndex = 1
    Caption = 'Put mode'
    OnClick = SpeedButton3Click
  end
  object StartBut: TSpeedButton
    Left = 32
    Top = 40
    Width = 65
    Height = 22
    AllowAllUp = True
    GroupIndex = 2
    Caption = 'GO'
    OnClick = StartButClick
  end
  object Label4: TLabel
    Left = 0
    Top = 24
    Width = 118
    Height = 13
    Caption = '--Particle Settings:---'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 656
    Top = 32
    Width = 110
    Height = 13
    BiDiMode = bdRightToLeft
    Caption = '--Physics Settings:--------'
    ParentBiDiMode = False
  end
  object Label6: TLabel
    Left = 656
    Top = 240
    Width = 114
    Height = 13
    BiDiMode = bdRightToLeft
    Caption = '--Bounder Settings:--------'
    ParentBiDiMode = False
  end
  object vDivFlash: TShape
    Left = 754
    Top = 63
    Width = 17
    Height = 17
    Hint = 'Press To Reset Value'
    Brush.Color = clLime
    ParentShowHint = False
    Pen.Style = psDot
    Pen.Width = 2
    Shape = stCircle
    ShowHint = True
    OnMouseDown = vDivFlashMouseDown
  end
  object fMulFlash: TShape
    Left = 754
    Top = 103
    Width = 17
    Height = 17
    Hint = 'Press To Reset Value'
    Brush.Color = clLime
    ParentShowHint = False
    Pen.Style = psDot
    Pen.Width = 2
    Shape = stCircle
    ShowHint = True
    OnMouseDown = fMulFlashMouseDown
  end
  object qMulFlash: TShape
    Left = 754
    Top = 143
    Width = 17
    Height = 17
    Hint = 'Press To Reset Value'
    Brush.Color = clLime
    ParentShowHint = False
    Pen.Style = psDot
    Pen.Width = 2
    Shape = stCircle
    ShowHint = True
    OnMouseDown = qMulFlashMouseDown
  end
  object cMulFlash: TShape
    Left = 754
    Top = 183
    Width = 17
    Height = 17
    Hint = 'Press To Reset Value'
    Brush.Color = clLime
    ParentShowHint = False
    Pen.Style = psDot
    Pen.Width = 2
    Shape = stCircle
    ShowHint = True
    OnMouseDown = cMulFlashMouseDown
  end
  object Label2: TLabel
    Left = 704
    Top = 320
    Width = 39
    Height = 13
    Caption = 'Zoomer:'
  end
  object BndRFlash: TShape
    Left = 738
    Top = 271
    Width = 17
    Height = 17
    Hint = 'Press To Reset Value'
    Brush.Color = clLime
    ParentShowHint = False
    Pen.Style = psDot
    Pen.Width = 2
    Shape = stCircle
    ShowHint = True
    OnMouseDown = BndRFlashMouseDown
  end
  object Label3: TLabel
    Left = 664
    Top = 304
    Width = 116
    Height = 13
    Caption = '--Display Settings:---'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 672
    Top = 464
    Width = 104
    Height = 13
    Caption = '--Array Settings:---'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object NavImage: TImage
    Left = 19
    Top = 328
    Width = 81
    Height = 81
  end
  object Bevel3: TBevel
    Left = 16
    Top = 325
    Width = 89
    Height = 89
  end
  object ChaosLabel: TLabel
    Left = 248
    Top = 0
    Width = 58
    Height = 13
    Caption = 'Chaos level:'
  end
  object BegArrBut: TButton
    Left = 20
    Top = 429
    Width = 17
    Height = 17
    Caption = '0'
    TabOrder = 43
    OnClick = BegArrButClick
  end
  object Xscroll: TScrollBar
    Left = 129
    Top = 516
    Width = 497
    Height = 17
    Max = 200
    PageSize = 10
    Position = 100
    TabOrder = 0
    OnChange = XscrollChange
  end
  object Yscroll: TScrollBar
    Left = 627
    Top = 16
    Width = 17
    Height = 500
    Kind = sbVertical
    Max = 200
    PageSize = 10
    Position = 100
    TabOrder = 1
    OnChange = YscrollChange
  end
  object pRadEd: TLabeledEdit
    Left = 8
    Top = 208
    Width = 49
    Height = 17
    AutoSize = False
    BiDiMode = bdLeftToRight
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdRightToLeftNoAlign
    EditLabel.Caption = 'Radius:'
    EditLabel.ParentBiDiMode = False
    EditLabel.Layout = tlBottom
    LabelPosition = lpAbove
    LabelSpacing = 3
    ParentBiDiMode = False
    TabOrder = 2
    Text = '0'
  end
  object pAngEd: TLabeledEdit
    Left = 64
    Top = 144
    Width = 49
    Height = 21
    EditLabel.Width = 30
    EditLabel.Height = 13
    EditLabel.Caption = 'Angle:'
    LabelPosition = lpLeft
    LabelSpacing = 1
    TabOrder = 3
  end
  object pVelEd: TLabeledEdit
    Left = 64
    Top = 160
    Width = 49
    Height = 21
    EditLabel.Width = 37
    EditLabel.Height = 13
    EditLabel.Caption = 'Velocity'
    LabelPosition = lpLeft
    LabelSpacing = 1
    TabOrder = 4
  end
  object pQEd: TLabeledEdit
    Left = 64
    Top = 208
    Width = 49
    Height = 21
    EditLabel.Width = 49
    EditLabel.Height = 13
    EditLabel.Caption = 'Charge(q):'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 5
    Text = '0'
  end
  object QprCombo: TComboBox
    Left = 8
    Top = 280
    Width = 105
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = '(+) Positive'
    OnSelect = QprComboSelect
    Items.Strings = (
      '(+) Positive'
      '(-) Negative'
      '( 0 )Empty'
      '(><)OmniAtt'
      '(<>)OmniDist'
      '( -> )attractable'
      '(-< )distractable')
  end
  object pMassEd: TLabeledEdit
    Left = 36
    Top = 245
    Width = 49
    Height = 21
    EditLabel.Width = 28
    EditLabel.Height = 13
    EditLabel.Caption = 'Mass:'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 7
    Text = '0'
  end
  object pPropsEd: TCheckListBox
    Left = 8
    Top = 304
    Width = 73
    Height = 17
    ItemHeight = 13
    Items.Strings = (
      'Stationary')
    TabOrder = 8
  end
  object ClearScr: TButton
    Left = 688
    Top = 440
    Width = 75
    Height = 17
    Caption = 'ClearScreen'
    TabOrder = 9
    OnClick = ClearScrClick
  end
  object ZoomBar: TTrackBar
    Left = 680
    Top = 336
    Width = 89
    Height = 33
    LineSize = 3
    Max = 5000
    Min = 1
    Orientation = trHorizontal
    PageSize = 50
    Frequency = 1
    Position = 2500
    SelEnd = 0
    SelStart = 0
    TabOrder = 10
    ThumbLength = 17
    TickMarks = tmBoth
    TickStyle = tsManual
    OnChange = ZoomBarChange
  end
  object Button2: TButton
    Left = 712
    Top = 368
    Width = 25
    Height = 14
    Caption = '/\'
    TabOrder = 11
    OnClick = Button2Click
  end
  object vDivEd: TLabeledEdit
    Left = 688
    Top = 64
    Width = 65
    Height = 17
    Hint = 'Press To Reset Value'
    AutoSize = False
    BiDiMode = bdRightToLeft
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdRightToLeft
    EditLabel.Caption = 'Speed Divisor:'
    EditLabel.ParentBiDiMode = False
    LabelPosition = lpAbove
    LabelSpacing = 3
    ParentBiDiMode = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 12
    Text = '160'
    OnChange = vDivEdChange
    OnKeyPress = vDivEdKeyPress
  end
  object fMulEd: TLabeledEdit
    Left = 688
    Top = 104
    Width = 57
    Height = 17
    AutoSize = False
    EditLabel.Width = 81
    EditLabel.Height = 13
    EditLabel.Caption = 'Friction Multiplier:'
    LabelPosition = lpAbove
    LabelSpacing = 1
    TabOrder = 13
    Text = '0,99'
    OnChange = fMulEdChange
    OnKeyPress = fMulEdKeyPress
  end
  object qMulEd: TLabeledEdit
    Left = 688
    Top = 144
    Width = 65
    Height = 17
    AutoSize = False
    EditLabel.Width = 90
    EditLabel.Height = 13
    EditLabel.Caption = 'q-Charge Multiplier:'
    LabelPosition = lpAbove
    LabelSpacing = 1
    TabOrder = 14
    Text = '512'
    OnChange = qMulEdChange
    OnKeyPress = qMulEdKeyPress
  end
  object BndREd: TLabeledEdit
    Left = 688
    Top = 272
    Width = 49
    Height = 17
    AutoSize = False
    BiDiMode = bdLeftToRight
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdRightToLeftNoAlign
    EditLabel.Caption = 'Radius:'
    EditLabel.ParentBiDiMode = False
    EditLabel.Layout = tlBottom
    LabelPosition = lpAbove
    LabelSpacing = 3
    ParentBiDiMode = False
    TabOrder = 15
    Text = '1000'
    OnChange = BndREdChange
    OnKeyPress = BndREdKeyPress
  end
  object cMulEd: TLabeledEdit
    Left = 688
    Top = 184
    Width = 65
    Height = 17
    AutoSize = False
    EditLabel.Width = 107
    EditLabel.Height = 13
    EditLabel.Caption = 'Coll Energy Loss MUL:'
    LabelPosition = lpAbove
    LabelSpacing = 1
    TabOrder = 16
    Text = '0,99'
    OnChange = cMulEdChange
    OnKeyPress = cMulEdKeyPress
  end
  object vDivX2: TButton
    Left = 672
    Top = 64
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 17
    OnClick = vDivX2Click
  end
  object vDivX05: TButton
    Left = 656
    Top = 64
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 18
    OnClick = vDivX05Click
  end
  object Button3: TButton
    Left = 672
    Top = 104
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 19
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 656
    Top = 104
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 20
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 672
    Top = 144
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 21
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 656
    Top = 144
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 22
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 672
    Top = 184
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 23
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 656
    Top = 184
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 24
    OnClick = Button8Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 537
    Width = 795
    Height = 19
    Panels = <
      item
        Text = 'P'
        Width = 250
      end
      item
        Text = 'S'
        Width = 50
      end>
    SimplePanel = False
    SimpleText = 'sgsdgsdg'
  end
  object Button9: TButton
    Left = 40
    Top = 512
    Width = 49
    Height = 25
    Caption = 'About'
    TabOrder = 25
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 688
    Top = 488
    Width = 75
    Height = 17
    Caption = 'Destroy All'
    TabOrder = 26
    OnClick = Button10Click
  end
  object Button1: TButton
    Left = 672
    Top = 272
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 27
    OnClick = Button1Click
  end
  object Button11: TButton
    Left = 656
    Top = 272
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 28
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 760
    Top = 336
    Width = 17
    Height = 17
    Caption = '/2'
    TabOrder = 29
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 672
    Top = 336
    Width = 17
    Height = 17
    Caption = '/3'
    TabOrder = 30
    OnClick = Button13Click
  end
  object Button14: TButton
    Left = 760
    Top = 352
    Width = 17
    Height = 17
    Caption = 'x2'
    TabOrder = 31
    OnClick = Button14Click
  end
  object Button15: TButton
    Left = 672
    Top = 352
    Width = 17
    Height = 17
    Caption = 'x3'
    TabOrder = 32
    OnClick = Button15Click
  end
  object drFields: TCheckBox
    Left = 672
    Top = 384
    Width = 97
    Height = 17
    Caption = 'Draw Fields'
    TabOrder = 33
    OnClick = drFieldsClick
  end
  object drVels: TCheckBox
    Left = 672
    Top = 400
    Width = 97
    Height = 17
    Caption = 'Draw Velocity'
    TabOrder = 34
  end
  object drTrails: TCheckBox
    Left = 672
    Top = 416
    Width = 97
    Height = 17
    Caption = 'Trails'
    TabOrder = 35
  end
  object Button16: TButton
    Left = 688
    Top = 504
    Width = 75
    Height = 17
    Caption = 'Delete Last'
    TabOrder = 36
    OnClick = Button16Click
  end
  object frUpDown: TUpDown
    Left = 744
    Top = 104
    Width = 9
    Height = 16
    Min = 0
    Max = 2
    Position = 1
    TabOrder = 37
    Wrap = False
    OnMouseDown = frUpDownMouseDown
  end
  object Memo1: TMemo
    Left = 11
    Top = 448
    Width = 105
    Height = 65
    Alignment = taCenter
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.Strings = (
      'Particle '#8470': 2'
      'Positive'
      'Rad: 0,232 '
      'q: 2353'
      '--Stationary--')
    TabOrder = 38
  end
  object Button17: TButton
    Left = 41
    Top = 413
    Width = 41
    Height = 17
    Caption = 'INJECT'
    TabOrder = 39
    OnClick = Button17Click
  end
  object PrvArr: TButton
    Left = 36
    Top = 429
    Width = 25
    Height = 17
    Caption = '<'
    TabOrder = 40
    OnClick = PrvArrClick
  end
  object NxtArrBut: TButton
    Left = 60
    Top = 429
    Width = 25
    Height = 17
    Caption = '>'
    TabOrder = 41
    OnClick = NxtArrButClick
  end
  object EndArrBut: TButton
    Left = 84
    Top = 429
    Width = 17
    Height = 17
    Caption = '?'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Symbol'
    Font.Style = []
    ParentFont = False
    TabOrder = 44
    OnClick = EndArrButClick
  end
end
