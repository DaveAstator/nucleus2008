//---------------------------------------------------------------------------

#ifndef UltimatesH
#define UltimatesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <CheckLst.hpp>
#include <math.hpp>
#include <ComCtrls.hpp>
#include <StrUtils.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TScrollBar *Xscroll;
        TScrollBar *Yscroll;
        TImage *TheBoard;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TBevel *Bevel1;
        TSpeedButton *CommitBut;
   TLabeledEdit *pRadEd;
   TLabeledEdit *pAngEd;
   TLabeledEdit *pVelEd;
        TBevel *Bevel2;
   TLabeledEdit *pQEd;
        TComboBox *QprCombo;
        TLabel *Label1;
   TLabeledEdit *pMassEd;
   TCheckListBox *pPropsEd;
        TSpeedButton *SpeedButton3;
        TSpeedButton *StartBut;
   TButton *ClearScr;
   TTrackBar *ZoomBar;
   TButton *Button2;
   TLabel *Label4;
   TLabel *Label5;
   TLabeledEdit *vDivEd;
   TLabeledEdit *fMulEd;
   TLabeledEdit *qMulEd;
   TLabel *Label6;
   TLabeledEdit *BndREd;
   TLabeledEdit *cMulEd;
   TShape *vDivFlash;
   TButton *vDivX2;
   TButton *vDivX05;
   TShape *fMulFlash;
   TButton *Button3;
   TButton *Button4;
   TShape *qMulFlash;
   TShape *cMulFlash;
   TButton *Button5;
   TButton *Button6;
   TButton *Button7;
   TButton *Button8;
   TStatusBar *StatusBar1;
   TButton *Button9;
   TLabel *Label2;
   TButton *Button10;
   TButton *Button1;
   TButton *Button11;
   TShape *BndRFlash;
   TButton *Button12;
   TButton *Button13;
   TButton *Button14;
   TButton *Button15;
   TCheckBox *drFields;
   TCheckBox *drVels;
   TLabel *Label3;
   TCheckBox *drTrails;
   TLabel *Label7;
   TButton *Button16;
   TUpDown *frUpDown;
   TImage *NavImage;
   TMemo *Memo1;
   TButton *Button17;
   TButton *PrvArr;
   TButton *NxtArrBut;
   TButton *BegArrBut;
   TButton *EndArrBut;
   TBevel *Bevel3;
   TLabel *ChaosLabel;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall TheBoardMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall TheBoardMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall CommitButClick(TObject *Sender);
        void __fastcall StartButClick(TObject *Sender);
        void __fastcall ClearScrClick(TObject *Sender);
        void __fastcall YscrollChange(TObject *Sender);
        void __fastcall XscrollChange(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall ZoomBarChange(TObject *Sender);
   void __fastcall vDivEdKeyPress(TObject *Sender, char &Key);
   void __fastcall fMulEdKeyPress(TObject *Sender, char &Key);
   void __fastcall qMulEdKeyPress(TObject *Sender, char &Key);
   void __fastcall SpeedButton2Click(TObject *Sender);
   void __fastcall SpeedButton1Click(TObject *Sender);
   void __fastcall SpeedButton3Click(TObject *Sender);
   void __fastcall BndREdKeyPress(TObject *Sender, char &Key);
   void __fastcall BndREdChange(TObject *Sender);
   void __fastcall QprComboSelect(TObject *Sender);
   void __fastcall SetCMulClick(TObject *Sender);
   void __fastcall cMulEdKeyPress(TObject *Sender, char &Key);
   void __fastcall vDivEdChange(TObject *Sender);
   void __fastcall vDivX05Click(TObject *Sender);
   void __fastcall vDivX2Click(TObject *Sender);
   void __fastcall fMulEdChange(TObject *Sender);
   void __fastcall Button3Click(TObject *Sender);
   void __fastcall Button4Click(TObject *Sender);
   void __fastcall vDivFlashMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall fMulFlashMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall qMulFlashMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall cMulFlashMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall qMulEdChange(TObject *Sender);
   void __fastcall cMulEdChange(TObject *Sender);
   void __fastcall Button5Click(TObject *Sender);
   void __fastcall Button6Click(TObject *Sender);
   void __fastcall Button7Click(TObject *Sender);
   void __fastcall Button8Click(TObject *Sender);
   void __fastcall Button9Click(TObject *Sender);
   void __fastcall Button10Click(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall Button11Click(TObject *Sender);
   void __fastcall BndRFlashMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall Button12Click(TObject *Sender);
   void __fastcall Button13Click(TObject *Sender);
   void __fastcall Button14Click(TObject *Sender);
   void __fastcall Button15Click(TObject *Sender);
   void __fastcall drFieldsClick(TObject *Sender);
   void __fastcall frUpDownMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall BegArrButClick(TObject *Sender);
   void __fastcall EndArrButClick(TObject *Sender);
   void __fastcall PrvArrClick(TObject *Sender);
   void __fastcall NxtArrButClick(TObject *Sender);
   void __fastcall Button17Click(TObject *Sender);
   void __fastcall Button16Click(TObject *Sender);
private:	// User declarations
public:
        struct particle{
                double x,y,px,py;
                double ang;
                double vel,xvel,yvel;
                double rad,prad;
                double mass,pressure;
                double q;
                int qprop;
                double frict;
                bool stationary,par,anti,eater;
                void setang(){
                   if (xvel==0) ang=M_PI_2+(-Sign(yvel)+1)/2*M_PI;
                           else
                   if (xvel>0 && yvel<0)
                            ang=M_PI*2+atan(yvel/xvel);
                   else
                   if (xvel<0)
                            ang=M_PI+atan(yvel/xvel);
                    else
                           ang=atan(yvel/xvel);
                };
                void setvel(){
                   vel=sqrt(pow(xvel,2)+pow(yvel,2));
                }
                void setvxy(){
                xvel=vel*cos(ang);
                yvel=vel*sin(ang);
                }
                particle(){
                x=0;y=0;ang=0;vel=0;xvel=0;yvel=0;rad=0;mass=0;q=0;qprop=0;frict=0;
                stationary=false;
                par=false;
                anti=false;
                eater=false;
                }

                };
        struct pstack{
                particle pt;
                pstack *next;
                pstack *prev;
        };
        int pnum,navid;
        short emode;
        pstack *parr,*navarr;
        pstack *lastpt;
        double offX,offY,visW,visH,actW,actH;
        particle SetPt;
        bool going,drFieldsFT,drFieldsLT;
        double vKoeff,frKoeff,qMul,cMul;
        double bndX,bndY,bndR;
        particle p,p1,p2;
        byte DrawStep;
        int oX,oY,pX,pY;

		// User declarations
        __fastcall TForm1(TComponent* Owner);
        void CollideBoth(particle &pt1, particle &pt2);
        void AffectLeft(particle & pt1, particle & pt2);
        double atanxy(double x, double y);
        bool chkcollide(particle & pt1, particle & pt2);
        double dist(double x1, double y1, double x2, double y2);
        void DrawParticles(pstack *darr);
        void MakeAStep(void);
        double distp(particle p1, particle p2);
        void FieldAffectLeft(particle & pt1, particle & pt2);
   bool chkcollideWBound(particle & pt1);
void   AffectByBound(particle & pt1);
   void ZoomZoom(double k);
   void UpdateStatus(void);
   void setNavProps();

};

//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
