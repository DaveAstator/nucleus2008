//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "Ultimates_bak.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


        const TColor clPos=RGB(255,64,64);
        const TColor clNeg=RGB(128,198,255);
        const TColor clNeut=RGB(164,164,164);
        const TColor clAtt=RGB(164,255,164);
        const TColor clDist=RGB(164,0,164);
TColor ChargeC[5]={RGB(255,64,64),RGB(128,198,255),RGB(164,164,164),RGB(164,255,164),RGB(164,0,164)};


void TForm1::CollideBoth(particle &pt1, particle &pt2)
{
       //TODO: Add your source code here
}

void TForm1::AffectLeft(particle & pt1, particle & pt2)
{
double angbb,angrb,vsum;//,vxto1,vyto1;
pt1.setang();
pt1.setvel();
pt2.setang();
pt2.setvel();
angbb=atanxy(pt1.x-pt2.x,pt1.y-pt2.y);//angle between ptcs
angrb=angbb-pt2.ang; //ang between p2's speed and bb angle
if (!pt2.stationary){
vsum=abs(pt2.vel*cos(angrb)*(pt2.mass+0.001)); //projection of p2's speed on colLine
}
else
vsum=abs(pt1.vel*sin(angrb)*(pt1.mass+0.001)); //projection of p2's speed on colLine

pt1.xvel=(pt1.xvel+vsum*cos(angbb)/((pt1.mass)+0.001));
pt1.yvel=(pt1.yvel+vsum*sin(angbb)/(pt1.mass+0.001));
//vyto1=vsum*sin(angbb);
        //TODO: Add your source code here
}


void __fastcall TForm1::FormCreate(TObject *Sender)
{
emode = 2;
bndX=0;
bndY=0;
bndR=1000;
qMul=1;
qMulEd->Text=FloatToStr(qMul);
frKoeff=0.99;
vKoeff=160;
QprCombo->ItemIndex=0;
offX=0;
offY=0;
actW=TheBoard->Width;
actH=TheBoard->Height;
visW=actW;
visH=actH;
offX=-visW/2;
offY=-visH/2;

going=false;
parr=new pstack;
lastpt=parr;
pnum=0;
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
TheBoard->Parent->DoubleBuffered=true;
DrawStep=0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TheBoardMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
if (emode == 2){
   if (DrawStep==0) {
      //first click: creating empty place for nex pt;
      lastpt->next=new pstack;
      pnum++;
      Label2->Caption=IntToStr(pnum);
      lastpt->pt.qprop=QprCombo->ItemIndex;
   }
   if (DrawStep<3) {
      DrawStep++;
      pX=X;
      pY=Y;
   }
   else {
      lastpt->pt.setang() ;
      if (pPropsEd->Checked[0]) {
      lastpt->pt.stationary=true;
      lastpt->pt.xvel=0;
      lastpt->pt.yvel=0;
      }
      SetPt=lastpt->pt;
      lastpt=lastpt->next;
      DrawStep=0;
      pRadEd->Text=FloatToStr(SetPt.rad);
   }
   if (DrawStep==1) {
      //Begin of drawing pt size;
      TheBoard->Canvas->Pen->Style=psClear;
      oX=X;
      oY=Y;
      pX=X;
      pY=Y;
      lastpt->pt.x=X*actW/visW+offX;
      lastpt->pt.y=Y*actH/visH+offY;
   }
   if (DrawStep==2) {
         //Begin of drawing pt Efield; Assigning mass by radius
        lastpt->pt.mass=pow(lastpt->pt.rad,2)*M_PI+0.001;
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Color=clNeg;
        TheBoard->Canvas->Pen->Style=psDash;
        TheBoard->Canvas->Pen->Mode=pmXor;
        double rad;
        rad=sqrt(pow(pX-oX,2)+pow(pY-oY,2));
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
   }
   if (DrawStep==3) {
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Style=psSolid;
        TheBoard->Canvas->Pen->Color=clAtt;
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(pX,pY);
   }
}
if (emode == 1) {
      lastpt->next=new pstack;
      pnum++;
      SetPt.rad=StrToFloat(pRadEd->Text);
      SetPt.mass=StrToFloat(pMassEd->Text);
      SetPt.q=StrToFloat(pQEd->Text);
      SetPt.qprop=QprCombo->ItemIndex;
      lastpt->pt=SetPt;
      lastpt->pt.x=X*actW/visW+offX;
      lastpt->pt.y=Y*actH/visH+offY;
      lastpt->pt.setang();
      lastpt=lastpt->next;
      DrawParticles(parr);
}


}
//---------------------------------------------------------------------------
void __fastcall TForm1::TheBoardMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
if (emode == 2) {
   if (DrawStep==1){
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmXor;
        TheBoard->Canvas->Brush->Color=ChargeC[QprCombo->ItemIndex];
        double rad;
        rad=sqrt(pow(pX-oX,2)+pow(pY-oY,2))/2;
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
        rad=sqrt(pow(X-oX,2)+pow(Y-oY,2))/2;
        TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
        pX=X;
        pY=Y;
        lastpt->pt.rad=rad*actW/visW;
        pRadEd->Text=FloatToStr(rad*actW/visW);
        pMassEd->Text=FloatToStr(pow(rad*actW/visW,2)*M_PI);
   }
   else
   if (DrawStep==2){
          TheBoard->Canvas->Brush->Style=bsClear;
          TheBoard->Canvas->Pen->Color=clNeg;
          TheBoard->Canvas->Pen->Style=psDash;
          TheBoard->Canvas->Pen->Mode=pmXor;
        double rad;
          rad=sqrt(pow(pX-oX,2)+pow(pY-oY,2));
          TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
          rad=sqrt(pow(X-oX,2)+pow(Y-oY,2));
          TheBoard->Canvas->Ellipse(oX-rad,oY-rad,oX+rad,oY+rad);
          pX=X;
          pY=Y;
        if (lastpt->pt.qprop== 1) lastpt->pt.q=-rad*actH/visH;
        else
           lastpt->pt.q=rad*actW/visW;
        pQEd->Text=FloatToStr(lastpt->pt.q);
   }
   if (DrawStep==3){
        TheBoard->Canvas->Brush->Style=bsClear;
        TheBoard->Canvas->Pen->Style=psSolid;
        TheBoard->Canvas->Pen->Color=clAtt;
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(pX,pY);
        TheBoard->Canvas->MoveTo(oX,oY);
        TheBoard->Canvas->LineTo(X,Y);
        lastpt->pt.xvel=(X-oX)*actW/visW;
        lastpt->pt.yvel=(Y-oY)*actH/visH;
        pX=X;
        pY=Y;
      }
}


}
//---------------------------------------------------------------------------



double TForm1::atanxy(double x, double y)
{
double ang;
        if (x==0 && y==0) ang=0;
          else
        if (x==0)
                ang=M_PI_2+(-Sign(y)+1)/2*M_PI;
          else
        if (x>0 && y<0)
                ang=M_PI*2+atan(y/x);
          else
        if (x<0)
                ang=M_PI+atan(y/x);
          else
                ang=atan(y/x);
return ang;
        //TODO: Add your source code here
}

bool TForm1::chkcollide(particle & pt1,particle & pt2)
{
        if (dist(pt1.x,pt1.y,pt2.x,pt2.y) < (pt1.rad+pt2.rad+1)){
                if (dist(pt1.x+pt1.xvel/vKoeff,pt1.y+pt1.yvel/vKoeff,pt2.x+pt2.xvel/vKoeff,pt2.y+pt2.yvel/vKoeff) < (pt1.rad+pt2.rad+1))
                return true;
}
 else
  return false;

 }

double TForm1::dist(double x1, double y1, double x2, double y2)
{
return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
        //TODO: Add your source code here
}
void __fastcall TForm1::CommitButClick(TObject *Sender)
{
TheBoard->Parent->DoubleBuffered=true;
parr->next->pt.rad=7;
Label2->Caption=lastpt->pt.rad;
}
//---------------------------------------------------------------------------


void TForm1::DrawParticles(pstack *darr)
{
int i;
double dx,dy,dr;
pstack *tmp;
tmp=darr;
for (i=0;i<pnum;i++){
if (!((tmp->pt.x+tmp->pt.rad)<offX) || ((tmp->pt.x+tmp->pt.rad)>(offX+actW)) || ((tmp->pt.y+tmp->pt.rad)<(offY)) || ((tmp->pt.y+tmp->pt.rad)>(offY+actH))){
        TheBoard->Canvas->Brush->Color=clBlack;
        TheBoard->Canvas->Brush->Style=bsSolid;
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmCopy;
        dx=(tmp->pt.px-offX)*visW/actW;
        dy=(tmp->pt.py-offY)*visH/actH;
         dr=tmp->pt.rad*visW/actW;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        TheBoard->Canvas->Brush->Color=ChargeC[tmp->pt.qprop];
        dx=(tmp->pt.x-offX)*visW/actW;
        dy=(tmp->pt.y-offY)*visH/actH;
        dr=tmp->pt.rad*visW/actW;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        }
       // OLD CODE.

/* code with centers
if (!((tmp->pt.x+tmp->pt.rad)<cenX-visW*zoom/2)
|| ((tmp->pt.x-tmp->pt.rad)>cenX+visW*zoom/2)
|| ((tmp->pt.y+tmp->pt.rad)<cenY-visH*zoom/2)
|| ((tmp->pt.y-tmp->pt.rad)>cenY+visH*zoom/2)){
int offX=cenX-
int actx,acty;
actx=pt.x-cenX-visW*zoom/2;
acty=pt.y-cenY-visH*zoom/2;
        TheBoard->Canvas->Brush->Color=clBlack;
        TheBoard->Canvas->Brush->Style=bsSolid;
        TheBoard->Canvas->Pen->Style=psClear;
        TheBoard->Canvas->Pen->Mode=pmCopy;
        dx=actx*zoom;
        dy=acty*zoom;
        dr=tmp->pt.rad*zoom;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
        TheBoard->Canvas->Brush->Color=ChargeC[tmp->pt.qprop];
        dx=(tmp->pt.x-offX)*visW/actW;
        dy=(tmp->pt.y-offY)*visH/actH;
        dr=tmp->pt.rad*zoom;
        TheBoard->Canvas->Ellipse(dx-dr,dy-dr,dx+dr,dy+dr);
}                   */
tmp=tmp->next;
    }
//draw the bound;
    TheBoard->Canvas->Brush->Style=bsClear;
    TheBoard->Canvas->Pen->Style=psSolid;
    TheBoard->Canvas->Pen->Color=clWhite;
    TheBoard->Canvas->Ellipse((bndX-offX-bndR)*visW/actW,(bndY-offY-bndR)*visW/actW,(bndX-offX+bndR)*visW/actW,(bndY-offY+bndR)*visW/actW);
}
void __fastcall TForm1::StartButClick(TObject *Sender)
{
going=StartBut->Down;
if (!going) StartBut->Caption="GO";
if (going) {
 Form1->Caption="true";
MakeAStep();
 }
else Form1->Caption="false";

}
//---------------------------------------------------------------------------


void TForm1::MakeAStep(void)
{
pstack *tmp,*fut,*tfut;
int i,k,z;
while (going) {
   tmp=parr;
   fut=new pstack;
   tfut=fut;
   for (i=0;i<pnum;i++){     //creating future system here:
      tmp->pt.px=tmp->pt.x;  //  setting pxand 'ys in present sys. i dunno - why so?..
      tmp->pt.py=tmp->pt.y;
      tfut->pt=tmp->pt;      //copy contents
      tfut->next=new pstack; //    create next free slot
      tmp=tmp->next;         //go forward
      tfut=tfut->next;
   }
//after that step we have a copy of present system named fut.
//going to start.
   tmp=parr;
   tfut=fut;
   for (i=0;i<pnum;i++){
      //affecting curr pt in future system by
      // every paticle in present
     for (k=0;k<pnum;k++){
         if (k!=i){  //affecting velocity by collison with pres particle and by E-fields
            if (!tfut->pt.stationary){
           FieldAffectLeft(tfut->pt,tmp->pt);
      //       tfut->pt.xvel=tfut->pt.xvel-(tmp->pt.q*20)/pow(distp(tmp->pt,tfut->pt),2)*cos(atanxy(tmp->pt.x-tfut->pt.x,tmp->pt.y-tfut->pt.y));
      //      tfut->pt.yvel=tfut->pt.yvel-(tmp->pt.q*20)/pow(distp(tmp->pt,tfut->pt),2)*sin(atanxy(tmp->pt.x-tfut->pt.x,tmp->pt.y-tfut->pt.y));
            if(chkcollide(tfut->pt,tmp->pt)) AffectLeft(tfut->pt,tmp->pt);
         }
         }
      tmp=tmp->next;
      }
     if(chkcollideWBound(tfut->pt))
      if (!tfut->pt.stationary)
     {
     AffectByBound(tfut->pt);
     }
    tmp=parr; //go to beginning of present system
               //changing position of future particle
    tfut->pt.x=tfut->pt.x+tfut->pt.xvel/vKoeff;
    tfut->pt.y=tfut->pt.y+tfut->pt.yvel/vKoeff;
    tfut->pt.xvel=tfut->pt.xvel*frKoeff;
    tfut->pt.yvel=tfut->pt.yvel*frKoeff;
    tfut=tfut->next;          //changing to the next particle
}
//now we need to copy the contents of future into present:
  //delete parr;
  //parr=fut;
  //lastpt=parr;
tfut=fut;  //going to zeroes
tmp=parr;  //begin of pres which will be changed to the future
  for (k=0;k<pnum;k++){
      tmp->pt=tfut->pt;
      tmp=tmp->next;   //going forward.. WOW THAT WAS IT!
      tfut=tfut->next;
  }
//  lastpt->next=new pstack;
//  lastpt=lastpt->next;
Application->ProcessMessages();

DrawParticles(parr);
Label3->Caption="going";
delete fut;
if (!going) break;
}
Label3->Caption="stopped";


}
void __fastcall TForm1::Button1Click(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
}
//---------------------------------------------------------------------------


double TForm1::distp(particle p1, particle p2)
{
return sqrt(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2));
        //TODO: Add your source code here

}
void __fastcall TForm1::YscrollChange(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
offY=offY+Yscroll->Position-100;
Yscroll->Position=100;
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::XscrollChange(TObject *Sender)
{
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
offX=offX+Xscroll->Position-100;
Xscroll->Position=100;
DrawParticles(parr);
}
//---------------------------------------------------------------------------


void TForm1::FieldAffectLeft(particle &pt1, particle &pt2)
{
if ((pt1.qprop<2) && (pt2.qprop<2)){
   pt1.xvel=pt1.xvel-(pt2.q*pt1.q*qMul)/pow(distp(pt1,pt2),2)*cos(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
   pt1.yvel=pt1.yvel-(pt2.q*pt1.q*qMul)/pow(distp(pt1,pt2),2)*sin(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
}
else
if ((pt1.qprop*2==pt1.qprop+pt2.qprop) && (pt1.qprop<2)){
   pt1.xvel=pt1.xvel-abs(pt2.q*qMul)/pow(distp(pt1,pt2),2)*cos(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
   pt1.yvel=pt1.yvel-abs(pt2.q*qMul)/pow(distp(pt1,pt2),2)*sin(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
}
else
if (pt1.qprop==2){}
else
if ((pt1.qprop==3) || (pt2.qprop==3)){
   pt1.xvel=pt1.xvel+(abs(pt2.q*pt1.q)*qMul)/pow(distp(pt1,pt2),2)*cos(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
   pt1.yvel=pt1.yvel+(abs(pt2.q*pt1.q)*qMul)/pow(distp(pt1,pt2),2)*sin(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
}
else
if ((pt1.qprop==4) || (pt2.qprop==4)){
   pt1.xvel=pt1.xvel-(abs(pt2.q*pt1.q)*qMul)/pow(distp(pt1,pt2),2)*cos(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
   pt1.yvel=pt1.yvel-(abs(pt2.q*pt1.q)*qMul)/pow(distp(pt1,pt2),2)*sin(atanxy(pt2.x-pt1.x,pt2.y-pt1.y));
}




}

void __fastcall TForm1::Button2Click(TObject *Sender)
{
ZoomBar->Position=ZoomBar->Max/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ZoomBarChange(TObject *Sender)
{
offX=offX+(actW-visW)/2;
offY=offY+(actH-visH)/2;
actW=visW*ZoomBar->Position/(ZoomBar->Max/6);
actH=visH*ZoomBar->Position/(ZoomBar->Max/6);
offX=offX-(actW-visW)/2;
offY=offY-(actH-visH)/2;
TheBoard->Canvas->Brush->Color=clBlack;
TheBoard->Canvas->FillRect(ClientRect);
DrawParticles(parr);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::vDivEdKeyPress(TObject *Sender, char &Key)
{
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}


}
//---------------------------------------------------------------------------


void __fastcall TForm1::fMulEdKeyPress(TObject *Sender, char &Key)
{
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------



void __fastcall TForm1::vDivEdChange(TObject *Sender)
{
if (vDivEd->Text.Length()<1 || StrToFloat(vDivEd->Text)<=0 ){
  vDivEd->Text="1";
  vKoeff=1;
 }
 vKoeff=StrToFloat(vDivEd->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::fMulEdChange(TObject *Sender)
{
if (StrToFloat(fMulEd->Text)<=0 || fMulEd->Text.Length()<=0 ){
  fMulEd->Text="1";
 }
 frKoeff=StrToFloat(fMulEd->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::qMulEdKeyPress(TObject *Sender, char &Key)
{
if (Key == '.') Key =',';
if (Key == '-') {
qMul=-qMul;
qMulEd->Text=FloatToStr(qMul);
}
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::qMulEdChange(TObject *Sender)
{
 if (fMulEd->Text.Length()<=0){
  qMulEd->Text="1";
 }
  qMul=StrToFloat(qMulEd->Text);
 }
//---------------------------------------------------------------------------


bool TForm1::chkcollideWBound(particle & pt1)
{
         if (dist(pt1.x,pt1.y,bndX,bndY) > (bndR-pt1.rad-1)){
                if (dist(pt1.x+pt1.xvel/vKoeff,pt1.y+pt1.yvel/vKoeff,bndX,bndY) > ((bndR-pt1.rad-1)))
                return true;
}
 else
  return false;

   //TODO: Add your source code here
}

TForm1::AffectByBound(particle & pt1)
{
double angbb,angrb,vsum;//,vxto1,vyto1;
pt1.setang();
pt1.setvel();
angbb=atanxy(bndX-pt1.x,bndY-pt1.y);//angle between ptcs
angrb=angbb-pt1.ang; //ang between p2's speed and bb angle
vsum=abs(pt1.vel*sin(angrb)); //projection of p2's speed on colLine
pt1.x=pt1.x-pt1.xvel/vKoeff*1.5;
pt1.y=pt1.y-pt1.yvel/vKoeff*1.5;
pt1.xvel=(pt1.xvel+vsum*cos(angbb))*1.001;
pt1.yvel=(pt1.yvel+vsum*sin(angbb))*1.001;

   //TODO: Add your source code here

}
void __fastcall TForm1::SpeedButton2Click(TObject *Sender)
{
emode=3;   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton1Click(TObject *Sender)
{
emode=2;   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton3Click(TObject *Sender)
{
emode=1;   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BndREdKeyPress(TObject *Sender, char &Key)
{
if (Key == '.') Key =',';
if (!((Key >= '0') && (Key <= '9') || (Key == 8) || (Key==','))){
Key=NULL;
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::BndREdChange(TObject *Sender)
{
if (BndREd->Text.Length()<1 || StrToFloat(BndREd->Text)<=0 ){
  BndREd->Text="1";
 }
 bndR=StrToFloat(BndREd->Text);
DrawParticles(parr);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::QprComboSelect(TObject *Sender)
{
if (QprCombo->ItemIndex==0) pQEd->Text=FloatToStr(abs(StrToFloat(pQEd->Text)));
if (QprCombo->ItemIndex==1) pQEd->Text=FloatToStr(-abs(StrToFloat(pQEd->Text)));
}
//---------------------------------------------------------------------------

