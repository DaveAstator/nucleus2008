object Form1: TForm1
  Left = 130
  Top = 107
  Width = 865
  Height = 642
  Caption = 'Ultimate Particles'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 5
    Top = 40
    Width = 79
    Height = 74
    Shape = bsFrame
  end
  object TheBoard: TImage
    Left = 120
    Top = 8
    Width = 577
    Height = 577
    OnMouseDown = TheBoardMouseDown
    OnMouseMove = TheBoardMouseMove
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 64
    Width = 73
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Down = True
    Caption = 'Create mode'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 8
    Top = 88
    Width = 73
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'Edit Mode'
    OnClick = SpeedButton2Click
  end
  object CommitBut: TSpeedButton
    Left = 88
    Top = 51
    Width = 29
    Height = 54
    Caption = 'OK'
    OnClick = CommitButClick
  end
  object Bevel2: TBevel
    Left = 0
    Top = 160
    Width = 113
    Height = 9
    Shape = bsTopLine
  end
  object Label1: TLabel
    Left = 8
    Top = 240
    Width = 87
    Height = 13
    Caption = 'Charge Properties:'
  end
  object SpeedButton3: TSpeedButton
    Left = 8
    Top = 40
    Width = 73
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'Put mode'
    OnClick = SpeedButton3Click
  end
  object Label2: TLabel
    Left = 24
    Top = 528
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 24
    Top = 544
    Width = 32
    Height = 13
    Caption = 'Label3'
  end
  object StartBut: TSpeedButton
    Left = 32
    Top = 16
    Width = 65
    Height = 22
    AllowAllUp = True
    GroupIndex = 2
    Caption = 'GO'
    OnClick = StartButClick
  end
  object Label4: TLabel
    Left = 8
    Top = 0
    Width = 109
    Height = 13
    Caption = '--Particle Settings:--------'
  end
  object Label5: TLabel
    Left = 720
    Top = 8
    Width = 110
    Height = 13
    BiDiMode = bdRightToLeft
    Caption = '--Physics Settings:--------'
    ParentBiDiMode = False
  end
  object Label6: TLabel
    Left = 720
    Top = 192
    Width = 114
    Height = 13
    BiDiMode = bdRightToLeft
    Caption = '--Bounder Settings:--------'
    ParentBiDiMode = False
  end
  object Xscroll: TScrollBar
    Left = 120
    Top = 584
    Width = 577
    Height = 17
    Max = 200
    PageSize = 10
    Position = 100
    TabOrder = 0
    OnChange = XscrollChange
  end
  object Yscroll: TScrollBar
    Left = 696
    Top = 8
    Width = 17
    Height = 577
    Kind = sbVertical
    Max = 200
    PageSize = 10
    Position = 100
    TabOrder = 1
    OnChange = YscrollChange
  end
  object pRadEd: TLabeledEdit
    Left = 8
    Top = 184
    Width = 49
    Height = 17
    AutoSize = False
    BiDiMode = bdLeftToRight
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdRightToLeftNoAlign
    EditLabel.Caption = 'Radius:'
    EditLabel.ParentBiDiMode = False
    EditLabel.Layout = tlBottom
    LabelPosition = lpAbove
    LabelSpacing = 3
    ParentBiDiMode = False
    TabOrder = 2
  end
  object pAngEd: TLabeledEdit
    Left = 64
    Top = 120
    Width = 49
    Height = 21
    EditLabel.Width = 30
    EditLabel.Height = 13
    EditLabel.Caption = 'Angle:'
    LabelPosition = lpLeft
    LabelSpacing = 1
    TabOrder = 3
  end
  object pVelEd: TLabeledEdit
    Left = 64
    Top = 136
    Width = 49
    Height = 21
    EditLabel.Width = 37
    EditLabel.Height = 13
    EditLabel.Caption = 'Velocity'
    LabelPosition = lpLeft
    LabelSpacing = 1
    TabOrder = 4
  end
  object pQEd: TLabeledEdit
    Left = 64
    Top = 184
    Width = 49
    Height = 21
    EditLabel.Width = 49
    EditLabel.Height = 13
    EditLabel.Caption = 'Charge(q):'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 5
  end
  object QprCombo: TComboBox
    Left = 8
    Top = 256
    Width = 105
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = '(+) Positive'
    OnSelect = QprComboSelect
    Items.Strings = (
      '(+) Positive'
      '(-) Negative'
      '( 0 )Empty'
      '(><)OmniAtt'
      '(<>)OmniDist'
      '( -> )attractable'
      '(-< )distractable')
  end
  object pMassEd: TLabeledEdit
    Left = 36
    Top = 221
    Width = 49
    Height = 21
    EditLabel.Width = 28
    EditLabel.Height = 13
    EditLabel.Caption = 'Mass:'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 7
  end
  object pPropsEd: TCheckListBox
    Left = 8
    Top = 280
    Width = 105
    Height = 137
    ItemHeight = 13
    Items.Strings = (
      'Stationary'
      'Affectable'
      'Affects others'
      'Collidable'
      '//Antimatter'
      '//Eater')
    TabOrder = 8
  end
  object Button1: TButton
    Left = 24
    Top = 488
    Width = 75
    Height = 25
    Caption = 'clear'
    TabOrder = 9
    OnClick = Button1Click
  end
  object ZoomBar: TTrackBar
    Left = 16
    Top = 424
    Width = 89
    Height = 33
    LineSize = 3
    Max = 5000
    Min = 1
    Orientation = trHorizontal
    PageSize = 50
    Frequency = 1
    Position = 2500
    SelEnd = 0
    SelStart = 0
    TabOrder = 10
    ThumbLength = 17
    TickMarks = tmBoth
    TickStyle = tsManual
    OnChange = ZoomBarChange
  end
  object Button2: TButton
    Left = 48
    Top = 456
    Width = 25
    Height = 17
    Caption = '/\'
    TabOrder = 11
    OnClick = Button2Click
  end
  object vDivEd: TLabeledEdit
    Left = 728
    Top = 40
    Width = 73
    Height = 21
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.Caption = 'Speed Divisor:'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 12
    Text = '160'
    OnChange = vDivEdChange
    OnKeyPress = vDivEdKeyPress
  end
  object fMulEd: TLabeledEdit
    Left = 728
    Top = 80
    Width = 73
    Height = 21
    EditLabel.Width = 125
    EditLabel.Height = 13
    EditLabel.Caption = 'Friction Multiplier(1=no fric)'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 13
    Text = '0,99'
    OnChange = fMulEdChange
    OnKeyPress = fMulEdKeyPress
  end
  object qMulEd: TLabeledEdit
    Left = 728
    Top = 120
    Width = 73
    Height = 21
    EditLabel.Width = 81
    EditLabel.Height = 13
    EditLabel.Caption = 'Charge Multiplier:'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 14
    Text = '20'
    OnChange = qMulEdChange
    OnKeyPress = qMulEdKeyPress
  end
  object BndREd: TLabeledEdit
    Left = 720
    Top = 224
    Width = 49
    Height = 17
    AutoSize = False
    BiDiMode = bdLeftToRight
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.BiDiMode = bdRightToLeftNoAlign
    EditLabel.Caption = 'Radius:'
    EditLabel.ParentBiDiMode = False
    EditLabel.Layout = tlBottom
    LabelPosition = lpAbove
    LabelSpacing = 3
    ParentBiDiMode = False
    TabOrder = 15
    Text = '1000'
    OnChange = BndREdChange
    OnKeyPress = BndREdKeyPress
  end
end
