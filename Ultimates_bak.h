//---------------------------------------------------------------------------

#ifndef Ultimates_bakH
#define Ultimates_bakH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <CheckLst.hpp>
#include <math.hpp>
#include <ComCtrls.hpp>
#include <StrUtils.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TScrollBar *Xscroll;
        TScrollBar *Yscroll;
        TImage *TheBoard;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TBevel *Bevel1;
        TSpeedButton *CommitBut;
   TLabeledEdit *pRadEd;
   TLabeledEdit *pAngEd;
   TLabeledEdit *pVelEd;
        TBevel *Bevel2;
   TLabeledEdit *pQEd;
        TComboBox *QprCombo;
        TLabel *Label1;
   TLabeledEdit *pMassEd;
   TCheckListBox *pPropsEd;
        TSpeedButton *SpeedButton3;
        TLabel *Label2;
        TLabel *Label3;
        TSpeedButton *StartBut;
        TButton *Button1;
   TTrackBar *ZoomBar;
   TButton *Button2;
   TLabel *Label4;
   TLabel *Label5;
   TLabeledEdit *vDivEd;
   TLabeledEdit *fMulEd;
   TLabeledEdit *qMulEd;
   TLabel *Label6;
   TLabeledEdit *BndREd;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall TheBoardMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall TheBoardMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall CommitButClick(TObject *Sender);
        void __fastcall StartButClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall YscrollChange(TObject *Sender);
        void __fastcall XscrollChange(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall ZoomBarChange(TObject *Sender);
   void __fastcall vDivEdKeyPress(TObject *Sender, char &Key);
   void __fastcall fMulEdKeyPress(TObject *Sender, char &Key);
   void __fastcall vDivEdChange(TObject *Sender);
   void __fastcall fMulEdChange(TObject *Sender);
   void __fastcall qMulEdKeyPress(TObject *Sender, char &Key);
   void __fastcall qMulEdChange(TObject *Sender);
   void __fastcall SpeedButton2Click(TObject *Sender);
   void __fastcall SpeedButton1Click(TObject *Sender);
   void __fastcall SpeedButton3Click(TObject *Sender);
   void __fastcall BndREdKeyPress(TObject *Sender, char &Key);
   void __fastcall BndREdChange(TObject *Sender);
   void __fastcall QprComboSelect(TObject *Sender);
private:	// User declarations
public:
        struct particle{
                double x,y,px,py;
                double ang;
                double vel,xvel,yvel;
                double rad,prad;
                double mass;
                double q;
                int qprop;
                double frict;
                bool stationary,par,anti,eater;
                void setang(){
                   if (xvel==0) ang=M_PI_2+(-Sign(yvel)+1)/2*M_PI;
                           else
                   if (xvel>0 && yvel<0)
                            ang=M_PI*2+atan(yvel/xvel);
                   else
                   if (xvel<0)
                            ang=M_PI+atan(yvel/xvel);
                    else
                           ang=atan(yvel/xvel);
                };
                void setvel(){
                   vel=sqrt(pow(xvel,2)+pow(yvel,2));
                }
                void setvxy(){
                xvel=vel*cos(ang);
                xvel=vel*sin(ang);
                }
                particle(){
                x=0;y=0;ang=0;vel=0;xvel=0;yvel=0;rad=0;mass=0;q=0;qprop=0;frict=0;
                stationary=false;
                par=false;
                anti=false;
                eater=false;
                }

                };
        struct pstack{
                particle pt;
                pstack *next;
                pstack *prev;
        };
        int pnum;
        short emode;
        pstack *parr;
        pstack *lastpt;
        double offX,offY,visW,visH,actW,actH;
        particle SetPt;
        bool going;
        double vKoeff,frKoeff,qMul;
        double bndX,bndY,bndR;
        particle p,p1,p2;
        byte DrawStep;
        int oX,oY,pX,pY;

		// User declarations
        __fastcall TForm1(TComponent* Owner);
        void CollideBoth(particle &pt1, particle &pt2);
        void AffectLeft(particle & pt1, particle & pt2);
        double atanxy(double x, double y);
        bool chkcollide(particle & pt1, particle & pt2);
        double dist(double x1, double y1, double x2, double y2);
        void DrawParticles(pstack *darr);
        void MakeAStep(void);
        double distp(particle p1, particle p2);
        void FieldAffectLeft(particle & pt1, particle & pt2);
   bool chkcollideWBound(particle & pt1);
   AffectByBound(particle & pt1);

};

//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
