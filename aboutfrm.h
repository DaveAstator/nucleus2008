//---------------------------------------------------------------------------

#ifndef aboutfrmH
#define aboutfrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TAboutForm : public TForm
{
__published:	// IDE-managed Components
   TImage *Image1;
   TSpeedButton *SpeedButton1;
   void __fastcall SpeedButton1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TAboutForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAboutForm *AboutForm;
//---------------------------------------------------------------------------
#endif
