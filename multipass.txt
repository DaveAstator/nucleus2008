void TForm1::MakeAStep(void)
{
pstack *tmp,*fut,*tfut;
int i,k,z;
while (going) {
   tmp=parr;
   fut=new pstack;
   tfut=fut;
   for (i=0;i<pnum;i++){     //creating future system here:
      tmp->pt.px=tmp->pt.x;  //  setting pxand 'ys in present sys. i dunno - why so?..
      tmp->pt.py=tmp->pt.y;
      tfut->pt=tmp->pt;      //copy contents
      tfut->next=new pstack; //    create next free slot
      tmp=tmp->next;         //go forward
      tfut=tfut->next;
   }
      //after that step we have a copy of present system named fut.
      //going to start.
//cycling collision chech for chain coollisions.
int z;
for (z=0;z<3;z++){
   tmp=parr;
   tfut=fut;
   for (i=0;i<pnum;i++){
     for (k=0;k<pnum;k++){
         if (k!=i){  //affecting velocity by collison with pres particle and by E-fields
            if (!tfut->pt.stationary){
              if(chkcollide(tfut->pt,tmp->pt)) AffectLeft(tfut->pt,tmp->pt);
            }
         }
      tmp=tmp->next;
     }
   tmp=parr;
   tfut=tfut->next;
   }
//reset of systems after one pass
   tfut=fut;
   tmp=parr;
     for (k=0;k<pnum;k++){
       tmp->pt=tfut->pt;
       tmp=tmp->next;   //going forward.. WOW THAT WAS IT!
       tfut=tfut->next;
     }
   tmp=parr;
   tfut=fut;
}

//cycle for field affection and border collisoons. and final setting of velocity and position
    tmp=parr;
    tfut=fut;
   for (i=0;i<pnum;i++)
   {
     for (k=0;k<pnum;k++)
      {
       if (k!=i)
          if (!tfut->pt.stationary){
            FieldAffectLeft(tfut->pt,tmp->pt);
            if(chkcollideWBound(tfut->pt))
              if (!tfut->pt.stationary)
                AffectByBound(tfut->pt);
          }
       tmp=tmp->next;
       }
    tfut->pt.x=tfut->pt.x+tfut->pt.xvel/vKoeff;
    tfut->pt.y=tfut->pt.y+tfut->pt.yvel/vKoeff;
    tfut->pt.xvel=tfut->pt.xvel*frKoeff;
    tfut->pt.yvel=tfut->pt.yvel*frKoeff;
    tfut=tfut->next;
    tmp=parr;
    }
//now we need to copy the contents of future into present:
tfut=fut;  //going to zeroes
tmp=parr;  //begin of pres which will be changed to the future
  for (k=0;k<pnum;k++){
      tmp->pt=tfut->pt;
      tmp=tmp->next;   //going forward.. WOW THAT WAS IT!
      tfut=tfut->next;
  }
Application->ProcessMessages();
DrawParticles(parr);
Label3->Caption="going";
delete fut;
if (!going) break;

}
Label3->Caption="stopped";


}